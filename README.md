# Assignment 1 - AWS, Terraform, CI/CD

This is for provisioning the infrastructure in AWS which will include ASG, ALB in default vpc of Singapore region

The EC2 servers after booting up will download its web content from a private S3 bucket

It contains application code - incremental and decremental counter

Changes to this website code will trigger CI/CD pipeline which will upload the code changes to private s3 bucket and then refresh the autoscaling group

ASG refresh would create another server before destroy till the entire group rolls out 

## Deliverables
1. Both public ALB and server fleet A will only allow on port 80/TCP

2. Server fleet will download and install nginx on boot

3. s3 bucket contains application code and server fleet will display the website

4. Application pipeline will upload the website and trigger the instance refresh of ASG

5. ASG refresh will update one instance at a time and wait for 300 seconds for each EC2 warmup. Entire refresh of 3 server fleet would take at least 15 mins

6. After successful refresh of ASG, the website can be reached via public internet

## improvements
1. Custom VPC, internet gateway, NAT gateway would be recommended for flexibility and privacy

2. Multiple private subnets within different AZs will enable the high availability of server fleet

3. NAT gateway is required and provisioned in the infrastructure to enable outbound internet otherwise server fleet will not be communicating with the internet

4. Two pipelines will be needed for separation of concerns: one for infrastructure and one for application code then every update will trigger corresponding pipeline

## infra code
Here are the commands to run the terraform code

First, default vpc should be planned to extract its data
```
terraform plan -target="aws_default_vpc.default" -out="vpc"
```

Apply the output plan of the target resource
```
terraform apply "vpc"
```

Now the entire infra can be applied
```
terraform apply -auto-approve
```

To destroy the entire infra
```
terraform apply -destroy -auto-approve
```

